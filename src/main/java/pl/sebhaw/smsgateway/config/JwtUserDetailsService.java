package pl.sebhaw.smsgateway.config;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class JwtUserDetailsService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        if ("user".equals(username)) {
            return new User("user", "$2y$12$RLizx4CyzeE0Tp4r0I9rBurYkJJfoOm9iJqCrKaQW/W5XJQjt8msC",
                    true, true, true, true, returnUserRole("ROLE_USER"));
        } else if("admin".equals(username)){
            return new User("admin", "$2y$12$Ig76vyr9ilRrJGWMIpsJmeo7s9sHDW.7/F4AEIx7wWE4PTintzRre",
                    true, true, true,true, returnUserRole("ROLE_ADMIN"));
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }

    private static List<GrantedAuthority> returnUserRole(String role) {
        List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
        roles.add(new SimpleGrantedAuthority(role));
        return roles;
    }
}
