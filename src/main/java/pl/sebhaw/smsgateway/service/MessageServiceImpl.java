package pl.sebhaw.smsgateway.service;

import jssc.SerialPortException;
import org.springframework.stereotype.Service;
import pl.sebhaw.smsgateway.connection.ModemConnection;

@Service
public class MessageServiceImpl implements MessageService{

    ModemConnection modemConnection = new ModemConnection();

    public MessageServiceImpl() throws SerialPortException, InterruptedException {
    }

    public void sendMessage(String phoneNumber, String message) {
        modemConnection.sendMessage(phoneNumber,message);
    }
}
