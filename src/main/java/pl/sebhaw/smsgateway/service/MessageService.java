package pl.sebhaw.smsgateway.service;

public interface MessageService {
    void sendMessage(String phoneNumber, String message);
}
