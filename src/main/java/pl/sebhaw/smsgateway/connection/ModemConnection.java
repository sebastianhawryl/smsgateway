package pl.sebhaw.smsgateway.connection;

import jssc.SerialPort;
import jssc.SerialPortException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@Slf4j
public class ModemConnection {

    SerialPort serialPort;
    byte newLine=0x0D;
    byte endOfLine= 0x1A;

    public ModemConnection() throws SerialPortException, InterruptedException {
        this.serialPort = new SerialPort("COM3");
        serialPort.openPort();
        serialPort.setParams(SerialPort.BAUDRATE_9600, SerialPort.DATABITS_8,SerialPort.STOPBITS_1
                            ,SerialPort.PARITY_NONE);
    }

    public void sendMessage(String phoneNumber, String message) {
        try {
            serialPort.writeString("AT+CMGF=1");
            Thread.sleep(1000);
            serialPort.writeByte(newLine);
            Thread.sleep(1000);

            serialPort.writeString("AT+CMGF=\""+ phoneNumber + "\"");
            Thread.sleep(1000);
            serialPort.writeByte(newLine);
            Thread.sleep(1000);

            serialPort.writeString(message);
            Thread.sleep(1000);
            serialPort.writeByte(newLine);
            Thread.sleep(1000);

            serialPort.writeByte(endOfLine);

            log.info("Message: " + message + " has been sent to " + phoneNumber + "on " + LocalDateTime.now());

        } catch(SerialPortException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
