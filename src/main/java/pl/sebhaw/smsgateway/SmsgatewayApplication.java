package pl.sebhaw.smsgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmsgatewayApplication {
	public static void main(String[] args) {
		SpringApplication.run(SmsgatewayApplication.class, args);
	}
}
