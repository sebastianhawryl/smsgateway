package pl.sebhaw.smsgateway.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.sebhaw.smsgateway.entity.MessageRequest;
import pl.sebhaw.smsgateway.service.IReturnPrincipal;
import pl.sebhaw.smsgateway.service.MessageService;

@RestController
@Slf4j
@CrossOrigin
public class MessageController implements IReturnPrincipal{

    private final MessageService messageService;

    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("/message")
    public ResponseEntity sendMessage(@RequestBody MessageRequest messageRequest) {
        messageService.sendMessage(messageRequest.getPhoneNumber(), messageRequest.getMessage());
        log.info("User with username: "+ getPrincipal() +" authenticated in Counter API");

        return ResponseEntity.ok().build();
    }
}
